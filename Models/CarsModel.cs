﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Laboratorni_2_8.Interfaces;

namespace Laboratorni_2_8.Models
{
    public class CarsModel
    {
        [Key]
        public int Id { get; set; }
        
        [StringLength(100)]
        [Required]
        public String Model { get; set; }
        
        [StringLength(100)]
        [Required]
        public String Brand { get; set; }
        
        [StringLength(100)]
        [Required]
        public String Year { get; set; }
        
        [StringLength(100)]
        [Required]
        public String Color { get; set; }
        
        [StringLength(1000)]
        [Required]
        public String DescriptionCar { get; set; }
        
        [Required]
        public int Price { get; set; }
        
        [Required]
        public bool IsRent { get; set; }
        
        public ICollection<ApplicationsModel> Application { get; set; }

        public void Update(IDataBaseCars car)
        {
            this.Model = car.Model;
            this.Brand = car.Brand;
            this.Year = car.Year;
            this.Color = car.Color;
            this.DescriptionCar = car.DescriptionCar;
            this.Price = car.Price;
            this.IsRent = car.IsRent;
        }
    }
}