﻿namespace Laboratorni_2_8.Models
{
    
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    
    public class UsersModel
    {
        [Key]
        public int Id { get; set; }
        
        [StringLength(100)]
        [Required]
        public String Login { get; set; }
        
        [StringLength(100)]
        [Required]
        public String Password { get; set; }
        
        [StringLength(100)]
        [Required]
        public String Name { get; set; }
        
        [StringLength(100)]
        [Required]
        public String Surname { get; set; }
        
        [StringLength(100)]
        [Required]
        public String Phone { get; set; }
        
        [StringLength(100)]
        [Required]
        public String Email { get; set; }
        
        [StringLength(100)]
        [Required]
        public String IdPassport { get; set; }
        
        [Required]
        public bool IsAdmin { get; set; }
        
        public ICollection<ApplicationsModel> Application { get; set; }
    }
}