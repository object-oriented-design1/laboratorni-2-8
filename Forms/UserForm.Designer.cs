﻿using System.ComponentModel;

namespace Laboratorni_2_8.Forms
{
    partial class UserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserForm));
            this.userMenu = new MaterialSkin.Controls.MaterialTabControl();
            this.homeTabPage = new System.Windows.Forms.TabPage();
            this.rentCarTabPage = new System.Windows.Forms.TabPage();
            this.userApplicationsTabPage = new System.Windows.Forms.TabPage();
            this.exitFromUserTabPage = new System.Windows.Forms.TabPage();
            this.imageListUserForm = new System.Windows.Forms.ImageList(this.components);
            this.settingsTabPage = new System.Windows.Forms.TabPage();
            this.userProfileTabPage = new System.Windows.Forms.TabPage();
            this.userMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // userMenu
            // 
            this.userMenu.Controls.Add(this.homeTabPage);
            this.userMenu.Controls.Add(this.rentCarTabPage);
            this.userMenu.Controls.Add(this.userApplicationsTabPage);
            this.userMenu.Controls.Add(this.settingsTabPage);
            this.userMenu.Controls.Add(this.userProfileTabPage);
            this.userMenu.Controls.Add(this.exitFromUserTabPage);
            this.userMenu.Depth = 0;
            this.userMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userMenu.ImageList = this.imageListUserForm;
            this.userMenu.Location = new System.Drawing.Point(3, 64);
            this.userMenu.MouseState = MaterialSkin.MouseState.HOVER;
            this.userMenu.Multiline = true;
            this.userMenu.Name = "userMenu";
            this.userMenu.SelectedIndex = 0;
            this.userMenu.Size = new System.Drawing.Size(795, 471);
            this.userMenu.TabIndex = 0;
            this.userMenu.SelectedIndexChanged += new System.EventHandler(this.ExitFromUserForm);
            // 
            // homeTabPage
            // 
            this.homeTabPage.ImageKey = "icons8-home-100.png";
            this.homeTabPage.Location = new System.Drawing.Point(4, 39);
            this.homeTabPage.Name = "homeTabPage";
            this.homeTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.homeTabPage.Size = new System.Drawing.Size(787, 428);
            this.homeTabPage.TabIndex = 0;
            this.homeTabPage.Text = "Головна сторінка";
            this.homeTabPage.UseVisualStyleBackColor = true;
            // 
            // rentCarTabPage
            // 
            this.rentCarTabPage.ImageKey = "icons8-car-rental-100.png";
            this.rentCarTabPage.Location = new System.Drawing.Point(4, 39);
            this.rentCarTabPage.Name = "rentCarTabPage";
            this.rentCarTabPage.Size = new System.Drawing.Size(787, 428);
            this.rentCarTabPage.TabIndex = 2;
            this.rentCarTabPage.Text = "Орендувати машину";
            this.rentCarTabPage.UseVisualStyleBackColor = true;
            // 
            // userApplicationsTabPage
            // 
            this.userApplicationsTabPage.ImageKey = "icons8-purchase-order-user-100.png";
            this.userApplicationsTabPage.Location = new System.Drawing.Point(4, 39);
            this.userApplicationsTabPage.Name = "userApplicationsTabPage";
            this.userApplicationsTabPage.Size = new System.Drawing.Size(787, 428);
            this.userApplicationsTabPage.TabIndex = 3;
            this.userApplicationsTabPage.Text = "Мої заявки";
            this.userApplicationsTabPage.UseVisualStyleBackColor = true;
            // 
            // exitFromUserTabPage
            // 
            this.exitFromUserTabPage.ImageKey = "icons8-logout-100.png";
            this.exitFromUserTabPage.Location = new System.Drawing.Point(4, 39);
            this.exitFromUserTabPage.Name = "exitFromUserTabPage";
            this.exitFromUserTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.exitFromUserTabPage.Size = new System.Drawing.Size(787, 428);
            this.exitFromUserTabPage.TabIndex = 1;
            this.exitFromUserTabPage.Text = "Вийти";
            this.exitFromUserTabPage.UseVisualStyleBackColor = true;
            // 
            // imageListUserForm
            // 
            this.imageListUserForm.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListUserForm.ImageStream")));
            this.imageListUserForm.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListUserForm.Images.SetKeyName(0, "icons8-logout-100.png");
            this.imageListUserForm.Images.SetKeyName(1, "icons8-home-100.png");
            this.imageListUserForm.Images.SetKeyName(2, "icons8-purchase-order-user-100.png");
            this.imageListUserForm.Images.SetKeyName(3, "icons8-car-rental-100.png");
            this.imageListUserForm.Images.SetKeyName(4, "icons8-gear-100.png");
            this.imageListUserForm.Images.SetKeyName(5, "icons8-customer-100.png");
            // 
            // settingsTabPage
            // 
            this.settingsTabPage.ImageKey = "icons8-gear-100.png";
            this.settingsTabPage.Location = new System.Drawing.Point(4, 39);
            this.settingsTabPage.Name = "settingsTabPage";
            this.settingsTabPage.Size = new System.Drawing.Size(787, 428);
            this.settingsTabPage.TabIndex = 4;
            this.settingsTabPage.Text = "Налаштування";
            this.settingsTabPage.UseVisualStyleBackColor = true;
            // 
            // userProfileTabPage
            // 
            this.userProfileTabPage.ImageKey = "icons8-customer-100.png";
            this.userProfileTabPage.Location = new System.Drawing.Point(4, 39);
            this.userProfileTabPage.Name = "userProfileTabPage";
            this.userProfileTabPage.Size = new System.Drawing.Size(787, 428);
            this.userProfileTabPage.TabIndex = 5;
            this.userProfileTabPage.Text = "Профіль";
            this.userProfileTabPage.UseVisualStyleBackColor = true;
            // 
            // UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 538);
            this.Controls.Add(this.userMenu);
            this.DrawerTabControl = this.userMenu;
            this.Name = "UserForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.UserForm_Load);
            this.userMenu.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.TabPage settingsTabPage;
        private System.Windows.Forms.TabPage userProfileTabPage;

        private System.Windows.Forms.TabPage userApplicationsTabPage;

        private System.Windows.Forms.TabPage rentCarTabPage;

        private System.Windows.Forms.ImageList imageListUserForm;

        private MaterialSkin.Controls.MaterialTabControl userMenu;
        private System.Windows.Forms.TabPage homeTabPage;
        private System.Windows.Forms.TabPage exitFromUserTabPage;

        #endregion
    }
}