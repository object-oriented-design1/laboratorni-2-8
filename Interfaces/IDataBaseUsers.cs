﻿namespace Laboratorni_2_8.Interfaces
{
    
    using System;
    using System.Collections.Generic;
    using Classes;
    using Models;
    
    public interface IDataBaseUsers
    {
        // Finctions for working with Table Users
        bool UserExists(String login, String password);
        bool LoginUserExists(String login);
        UsersModel AddUser(String login, String password,
            String name, String surname, String email, String phoneNumber, String idPassport);
        void AddUser(Client user, String login, String password);
        void DeleteUser(String login, String password);
        void UpdateUser(int idUser, String phone, String email);
        UsersModel SelectUser(int idUser);
        UsersModel SelectUser(String login, String password);
        List<UsersModel> SelectUsers();
    }
}