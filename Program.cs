﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Laboratorni_2_8.Forms;

namespace Laboratorni_2_8
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AttachConsole(-1);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AuthForm());
        }
        [DllImport("kernel32.dll")]
        static extern bool AttachConsole(int dwProcessId);
    }
}