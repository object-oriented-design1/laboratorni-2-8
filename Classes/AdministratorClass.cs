﻿using System;
using Laboratorni_2_8.Interfaces;
using Laboratorni_2_8.Models;

namespace Laboratorni_2_8.Classes
{
    public class Administrator : IHuman
    {
        private String Login { get; }
        private String Password { get; }
        private int UserId { get; set; }
        public String Name { get; set; }
        public String Surname { get; set; }
        public String PhoneNumber { get; set; }
        public String Email { get; set; }
        public String IdPassport { get; set; }
        public String FullName => Name + " " + Surname;
        
        public Administrator(UsersModel userModel)
        {
            UserId = userModel.Id;
            Login = userModel.Login;
            Password = userModel.Password;
            Name = userModel.Name;
            Surname = userModel.Surname;
            PhoneNumber = userModel.Phone;
            Email = userModel.Email;
            IdPassport = userModel.IdPassport;
        }

    }
}