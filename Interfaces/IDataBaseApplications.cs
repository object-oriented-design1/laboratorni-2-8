﻿namespace Laboratorni_2_8.Interfaces
{
    
    using System;
    using System.Collections.Generic;
    using Models;

    public interface IDataBaseApplications
    {
        // Functions for working with Table Orders
        void CreateNewApplication(ApplicationsModel applicationModel);
        ApplicationsModel SelectApplication(int idApplication);
        List<ApplicationsModel> SelectApplications();
        void UpdateApplication(int idApplication, String status, String notes);
        void UpdateApplication(int idApplication, String statusOrNotes);
        UsersModel GetUserApplications(int idApplication);
        CarsModel GetCarApplications(int idApplication);
        ApplicationsModel GetApplicationsByStatus(String status);
    }
}