﻿namespace Laboratorni_2_8.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Model = c.String(),
                        Brand = c.String(),
                        Year = c.String(),
                        Color = c.String(),
                        Price = c.Int(nullable: false),
                        IsRent = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Applications", "Status", c => c.String());
            AddColumn("dbo.Applications", "Notes", c => c.String());
            AddColumn("dbo.Applications", "LeaserTerm", c => c.String());
            AddColumn("dbo.Applications", "DateUpdate", c => c.DateTime());
            AddColumn("dbo.Applications", "UserId", c => c.Int(nullable: false));
            AddColumn("dbo.Applications", "CarId", c => c.Int(nullable: false));
            CreateIndex("dbo.Applications", "UserId");
            CreateIndex("dbo.Applications", "CarId");
            AddForeignKey("dbo.Applications", "CarId", "dbo.Cars", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Applications", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            DropColumn("dbo.Applications", "Name");
            DropColumn("dbo.Applications", "Description");
            DropColumn("dbo.Applications", "Version");
            DropColumn("dbo.Applications", "Author");
            DropColumn("dbo.Applications", "Link");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Applications", "Link", c => c.String());
            AddColumn("dbo.Applications", "Author", c => c.String());
            AddColumn("dbo.Applications", "Version", c => c.String());
            AddColumn("dbo.Applications", "Description", c => c.String());
            AddColumn("dbo.Applications", "Name", c => c.String());
            DropForeignKey("dbo.Applications", "UserId", "dbo.Users");
            DropForeignKey("dbo.Applications", "CarId", "dbo.Cars");
            DropIndex("dbo.Applications", new[] { "CarId" });
            DropIndex("dbo.Applications", new[] { "UserId" });
            DropColumn("dbo.Applications", "CarId");
            DropColumn("dbo.Applications", "UserId");
            DropColumn("dbo.Applications", "DateUpdate");
            DropColumn("dbo.Applications", "LeaserTerm");
            DropColumn("dbo.Applications", "Notes");
            DropColumn("dbo.Applications", "Status");
            DropTable("dbo.Cars");
        }
    }
}
