﻿namespace Laboratorni_2_8.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAtributes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Applications", "Status", c => c.String(maxLength: 100));
            AlterColumn("dbo.Applications", "Notes", c => c.String(maxLength: 300));
            AlterColumn("dbo.Applications", "LeaserTerm", c => c.DateTime());
            AlterColumn("dbo.Cars", "Model", c => c.String(maxLength: 100));
            AlterColumn("dbo.Cars", "Brand", c => c.String(maxLength: 100));
            AlterColumn("dbo.Cars", "Year", c => c.String(maxLength: 100));
            AlterColumn("dbo.Cars", "Color", c => c.String(maxLength: 100));
            AlterColumn("dbo.Users", "Login", c => c.String(maxLength: 100));
            AlterColumn("dbo.Users", "Password", c => c.String(maxLength: 100));
            AlterColumn("dbo.Users", "Name", c => c.String(maxLength: 100));
            AlterColumn("dbo.Users", "Surname", c => c.String(maxLength: 100));
            AlterColumn("dbo.Users", "Phone", c => c.String(maxLength: 100));
            AlterColumn("dbo.Users", "Email", c => c.String(maxLength: 100));
            AlterColumn("dbo.Users", "IdPassport", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "IdPassport", c => c.String());
            AlterColumn("dbo.Users", "Email", c => c.String());
            AlterColumn("dbo.Users", "Phone", c => c.String());
            AlterColumn("dbo.Users", "Surname", c => c.String());
            AlterColumn("dbo.Users", "Name", c => c.String());
            AlterColumn("dbo.Users", "Password", c => c.String());
            AlterColumn("dbo.Users", "Login", c => c.String());
            AlterColumn("dbo.Cars", "Color", c => c.String());
            AlterColumn("dbo.Cars", "Year", c => c.String());
            AlterColumn("dbo.Cars", "Brand", c => c.String());
            AlterColumn("dbo.Cars", "Model", c => c.String());
            AlterColumn("dbo.Applications", "LeaserTerm", c => c.String());
            AlterColumn("dbo.Applications", "Notes", c => c.String());
            AlterColumn("dbo.Applications", "Status", c => c.String());
        }
    }
}
