﻿using System.Data.Entity;
using Laboratorni_2_8.Models;

namespace Laboratorni_2_8.Contexts
{
    public class LaboratoryDbContext : DbContext
    {
        public LaboratoryDbContext()
            : base("mysql")
        {
            
        }
        
        public DbSet<UsersModel> UserDataBase { get; set; } 
        public DbSet<ApplicationsModel> ApplicationsDataBase { get; set; }
        public DbSet<CarsModel> CarDataBase { get; set; }
    }
}