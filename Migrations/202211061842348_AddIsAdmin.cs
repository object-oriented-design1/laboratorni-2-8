﻿namespace Laboratorni_2_8.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsAdmin : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IsAdmin", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "IsAdmin");
        }
    }
}
