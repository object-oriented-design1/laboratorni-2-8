﻿namespace Laboratorni_2_8.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Applications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.String(maxLength: 100, storeType: "nvarchar"),
                        Notes = c.String(maxLength: 300, storeType: "nvarchar"),
                        LeaserTerm = c.DateTime(precision: 0),
                        DateCreate = c.DateTime(precision: 0),
                        DateUpdate = c.DateTime(precision: 0),
                        UserId = c.Int(nullable: false),
                        CarId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cars", t => t.CarId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.CarId);
            
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Model = c.String(maxLength: 100, storeType: "nvarchar"),
                        Brand = c.String(maxLength: 100, storeType: "nvarchar"),
                        Year = c.String(maxLength: 100, storeType: "nvarchar"),
                        Color = c.String(maxLength: 100, storeType: "nvarchar"),
                        Price = c.Int(nullable: false),
                        IsRent = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        Password = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        Name = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        Surname = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        Phone = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        Email = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        IdPassport = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        IsAdmin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Applications", "UserId", "dbo.Users");
            DropForeignKey("dbo.Applications", "CarId", "dbo.Cars");
            DropIndex("dbo.Applications", new[] { "CarId" });
            DropIndex("dbo.Applications", new[] { "UserId" });
            DropTable("dbo.Users");
            DropTable("dbo.Cars");
            DropTable("dbo.Applications");
        }
    }
}
