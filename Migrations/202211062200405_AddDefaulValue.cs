﻿namespace Laboratorni_2_8.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDefaulValue : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "IsAdmin", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "IsAdmin", c => c.Boolean(nullable: false));
        }
    }
}
