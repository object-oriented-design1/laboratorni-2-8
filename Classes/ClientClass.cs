﻿namespace Laboratorni_2_8.Classes
{
    
    using System;
    using Interfaces;
    using Models;
    
    public class Client : IHuman
    {
        private String Login { get; }
        private String Password { get; }
        private int UserId { get; set; }
        public String Name { get; set; }
        public String Surname { get; set; }
        public String PhoneNumber { get; set; }
        public String Email { get; set; }
        public String IdPassport { get; set; }
        public bool IsAdmin { get; set; }
        public String FullName => Name + " " + Surname;

        public Client(UsersModel userModel)
        {
            UserId = userModel.Id;
            Login = userModel.Login;
            Password = userModel.Password;
            Name = userModel.Name;
            Surname = userModel.Surname;
            PhoneNumber = userModel.Phone;
            Email = userModel.Email;
            IdPassport = userModel.IdPassport;
            IsAdmin = userModel.IsAdmin;
        }

        public void ShowInfo()
        {
            Console.WriteLine(@"ID: " + UserId);
            Console.WriteLine(@"Login: " + Login);
            Console.WriteLine(@"Password: " + Password);
            Console.WriteLine(@"Name: " + Name);
            Console.WriteLine(@"Surname: " + Surname);
            Console.WriteLine(@"Phone number: " + PhoneNumber);
            Console.WriteLine(@"Email: " + Email);
            Console.WriteLine(@"Id passport: " + IdPassport);
            Console.WriteLine(@"Is Admin: " + IsAdmin);
        }

        public void SyncWithDataBase(IDataBaseUsers dataBaseUsers)
        {
            
        }
    }
}