﻿using System.ComponentModel;

namespace Laboratorni_2_8.Forms
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminForm));
            this.adminMenu = new MaterialSkin.Controls.MaterialTabControl();
            this.homeTabPage = new System.Windows.Forms.TabPage();
            this.addCarTabPage = new System.Windows.Forms.TabPage();
            this.materialMultiLineTextBox21 = new MaterialSkin.Controls.MaterialMultiLineTextBox2();
            this.materialTextBox1 = new MaterialSkin.Controls.MaterialTextBox();
            this.register_btn = new MaterialSkin.Controls.MaterialButton();
            this.PriceRent = new MaterialSkin.Controls.MaterialTextBox2();
            this.CarColor = new MaterialSkin.Controls.MaterialTextBox2();
            this.CarModel = new MaterialSkin.Controls.MaterialTextBox2();
            this.CarBrand = new MaterialSkin.Controls.MaterialTextBox2();
            this.CarYear = new MaterialSkin.Controls.MaterialTextBox2();
            this.applicationsTabPage = new System.Windows.Forms.TabPage();
            this.usersTabPage = new System.Windows.Forms.TabPage();
            this.settingsTabPage = new System.Windows.Forms.TabPage();
            this.exitFromAdminTabPage = new System.Windows.Forms.TabPage();
            this.imageListAdminForm = new System.Windows.Forms.ImageList(this.components);
            this.adminMenu.SuspendLayout();
            this.addCarTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // adminMenu
            // 
            this.adminMenu.Controls.Add(this.homeTabPage);
            this.adminMenu.Controls.Add(this.addCarTabPage);
            this.adminMenu.Controls.Add(this.applicationsTabPage);
            this.adminMenu.Controls.Add(this.usersTabPage);
            this.adminMenu.Controls.Add(this.settingsTabPage);
            this.adminMenu.Controls.Add(this.exitFromAdminTabPage);
            this.adminMenu.Depth = 0;
            this.adminMenu.ImageList = this.imageListAdminForm;
            this.adminMenu.Location = new System.Drawing.Point(4, 79);
            this.adminMenu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.adminMenu.MouseState = MaterialSkin.MouseState.HOVER;
            this.adminMenu.Multiline = true;
            this.adminMenu.Name = "adminMenu";
            this.adminMenu.SelectedIndex = 0;
            this.adminMenu.Size = new System.Drawing.Size(975, 672);
            this.adminMenu.TabIndex = 0;
            this.adminMenu.SelectedIndexChanged += new System.EventHandler(this.ExitFromAdminForm);
            // 
            // homeTabPage
            // 
            this.homeTabPage.ImageKey = "icons8-home-100.png";
            this.homeTabPage.Location = new System.Drawing.Point(4, 39);
            this.homeTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.homeTabPage.Name = "homeTabPage";
            this.homeTabPage.Padding = new System.Windows.Forms.Padding(4);
            this.homeTabPage.Size = new System.Drawing.Size(967, 629);
            this.homeTabPage.TabIndex = 3;
            this.homeTabPage.Text = "Головна сторінка";
            this.homeTabPage.UseVisualStyleBackColor = true;
            // 
            // addCarTabPage
            // 
            this.addCarTabPage.Controls.Add(this.materialMultiLineTextBox21);
            this.addCarTabPage.Controls.Add(this.materialTextBox1);
            this.addCarTabPage.Controls.Add(this.register_btn);
            this.addCarTabPage.Controls.Add(this.PriceRent);
            this.addCarTabPage.Controls.Add(this.CarColor);
            this.addCarTabPage.Controls.Add(this.CarModel);
            this.addCarTabPage.Controls.Add(this.CarBrand);
            this.addCarTabPage.Controls.Add(this.CarYear);
            this.addCarTabPage.ImageKey = "icons8-add-new-100.png";
            this.addCarTabPage.Location = new System.Drawing.Point(4, 39);
            this.addCarTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addCarTabPage.Name = "addCarTabPage";
            this.addCarTabPage.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addCarTabPage.Size = new System.Drawing.Size(967, 629);
            this.addCarTabPage.TabIndex = 0;
            this.addCarTabPage.Text = "Додати машину";
            this.addCarTabPage.UseVisualStyleBackColor = true;
            // 
            // materialMultiLineTextBox21
            // 
            this.materialMultiLineTextBox21.AnimateReadOnly = false;
            this.materialMultiLineTextBox21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.materialMultiLineTextBox21.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.materialMultiLineTextBox21.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.materialMultiLineTextBox21.Depth = 0;
            this.materialMultiLineTextBox21.HideSelection = true;
            this.materialMultiLineTextBox21.Location = new System.Drawing.Point(418, 249);
            this.materialMultiLineTextBox21.MaxLength = 32767;
            this.materialMultiLineTextBox21.MouseState = MaterialSkin.MouseState.OUT;
            this.materialMultiLineTextBox21.Name = "materialMultiLineTextBox21";
            this.materialMultiLineTextBox21.PasswordChar = '\0';
            this.materialMultiLineTextBox21.ReadOnly = false;
            this.materialMultiLineTextBox21.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.materialMultiLineTextBox21.SelectedText = "";
            this.materialMultiLineTextBox21.SelectionLength = 0;
            this.materialMultiLineTextBox21.SelectionStart = 0;
            this.materialMultiLineTextBox21.ShortcutsEnabled = true;
            this.materialMultiLineTextBox21.Size = new System.Drawing.Size(290, 168);
            this.materialMultiLineTextBox21.TabIndex = 21;
            this.materialMultiLineTextBox21.TabStop = false;
            this.materialMultiLineTextBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.materialMultiLineTextBox21.UseSystemPasswordChar = false;
            // 
            // materialTextBox1
            // 
            this.materialTextBox1.AnimateReadOnly = false;
            this.materialTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialTextBox1.Depth = 0;
            this.materialTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialTextBox1.LeadingIcon = null;
            this.materialTextBox1.Location = new System.Drawing.Point(540, 443);
            this.materialTextBox1.MaxLength = 50;
            this.materialTextBox1.MouseState = MaterialSkin.MouseState.OUT;
            this.materialTextBox1.Multiline = false;
            this.materialTextBox1.Name = "materialTextBox1";
            this.materialTextBox1.Size = new System.Drawing.Size(385, 50);
            this.materialTextBox1.TabIndex = 19;
            this.materialTextBox1.Text = "";
            this.materialTextBox1.TrailingIcon = null;
            // 
            // register_btn
            // 
            this.register_btn.AutoSize = false;
            this.register_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.register_btn.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Dense;
            this.register_btn.Depth = 0;
            this.register_btn.HighEmphasis = true;
            this.register_btn.Icon = null;
            this.register_btn.Location = new System.Drawing.Point(322, 552);
            this.register_btn.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.register_btn.MouseState = MaterialSkin.MouseState.HOVER;
            this.register_btn.Name = "register_btn";
            this.register_btn.NoAccentTextColor = System.Drawing.Color.Empty;
            this.register_btn.Size = new System.Drawing.Size(260, 59);
            this.register_btn.TabIndex = 16;
            this.register_btn.Text = "Реєстрація";
            this.register_btn.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.register_btn.UseAccentColor = false;
            this.register_btn.UseVisualStyleBackColor = true;
            // 
            // PriceRent
            // 
            this.PriceRent.AnimateReadOnly = false;
            this.PriceRent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PriceRent.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.PriceRent.Depth = 0;
            this.PriceRent.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.PriceRent.HideSelection = true;
            this.PriceRent.Hint = "Ціна оренди";
            this.PriceRent.LeadingIcon = null;
            this.PriceRent.Location = new System.Drawing.Point(323, 107);
            this.PriceRent.Margin = new System.Windows.Forms.Padding(4);
            this.PriceRent.MaxLength = 32767;
            this.PriceRent.MouseState = MaterialSkin.MouseState.OUT;
            this.PriceRent.Name = "PriceRent";
            this.PriceRent.PasswordChar = '\0';
            this.PriceRent.PrefixSuffixText = null;
            this.PriceRent.ReadOnly = false;
            this.PriceRent.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PriceRent.SelectedText = "";
            this.PriceRent.SelectionLength = 0;
            this.PriceRent.SelectionStart = 0;
            this.PriceRent.ShortcutsEnabled = true;
            this.PriceRent.Size = new System.Drawing.Size(259, 48);
            this.PriceRent.TabIndex = 13;
            this.PriceRent.TabStop = false;
            this.PriceRent.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.PriceRent.TrailingIcon = null;
            this.PriceRent.UseSystemPasswordChar = false;
            // 
            // CarColor
            // 
            this.CarColor.AnimateReadOnly = false;
            this.CarColor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CarColor.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.CarColor.Depth = 0;
            this.CarColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.CarColor.HideSelection = true;
            this.CarColor.Hint = "Колір машини";
            this.CarColor.LeadingIcon = null;
            this.CarColor.Location = new System.Drawing.Point(641, 29);
            this.CarColor.Margin = new System.Windows.Forms.Padding(4);
            this.CarColor.MaxLength = 32767;
            this.CarColor.MouseState = MaterialSkin.MouseState.OUT;
            this.CarColor.Name = "CarColor";
            this.CarColor.PasswordChar = '\0';
            this.CarColor.PrefixSuffixText = null;
            this.CarColor.ReadOnly = false;
            this.CarColor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CarColor.SelectedText = "";
            this.CarColor.SelectionLength = 0;
            this.CarColor.SelectionStart = 0;
            this.CarColor.ShortcutsEnabled = true;
            this.CarColor.Size = new System.Drawing.Size(261, 48);
            this.CarColor.TabIndex = 12;
            this.CarColor.TabStop = false;
            this.CarColor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.CarColor.TrailingIcon = null;
            this.CarColor.UseSystemPasswordChar = false;
            // 
            // CarModel
            // 
            this.CarModel.AnimateReadOnly = false;
            this.CarModel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CarModel.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.CarModel.Depth = 0;
            this.CarModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.CarModel.HideSelection = true;
            this.CarModel.Hint = "Модель машини";
            this.CarModel.LeadingIcon = null;
            this.CarModel.Location = new System.Drawing.Point(26, 29);
            this.CarModel.Margin = new System.Windows.Forms.Padding(4);
            this.CarModel.MaxLength = 32767;
            this.CarModel.MouseState = MaterialSkin.MouseState.OUT;
            this.CarModel.Name = "CarModel";
            this.CarModel.PasswordChar = '\0';
            this.CarModel.PrefixSuffixText = null;
            this.CarModel.ReadOnly = false;
            this.CarModel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CarModel.SelectedText = "";
            this.CarModel.SelectionLength = 0;
            this.CarModel.SelectionStart = 0;
            this.CarModel.ShortcutsEnabled = true;
            this.CarModel.Size = new System.Drawing.Size(259, 48);
            this.CarModel.TabIndex = 9;
            this.CarModel.TabStop = false;
            this.CarModel.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.CarModel.TrailingIcon = null;
            this.CarModel.UseSystemPasswordChar = false;
            // 
            // CarBrand
            // 
            this.CarBrand.AnimateReadOnly = false;
            this.CarBrand.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CarBrand.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.CarBrand.Depth = 0;
            this.CarBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.CarBrand.HideSelection = true;
            this.CarBrand.Hint = "Бренд машини";
            this.CarBrand.LeadingIcon = null;
            this.CarBrand.Location = new System.Drawing.Point(322, 29);
            this.CarBrand.Margin = new System.Windows.Forms.Padding(4);
            this.CarBrand.MaxLength = 32767;
            this.CarBrand.MouseState = MaterialSkin.MouseState.OUT;
            this.CarBrand.Name = "CarBrand";
            this.CarBrand.PasswordChar = '\0';
            this.CarBrand.PrefixSuffixText = null;
            this.CarBrand.ReadOnly = false;
            this.CarBrand.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CarBrand.SelectedText = "";
            this.CarBrand.SelectionLength = 0;
            this.CarBrand.SelectionStart = 0;
            this.CarBrand.ShortcutsEnabled = true;
            this.CarBrand.Size = new System.Drawing.Size(261, 48);
            this.CarBrand.TabIndex = 11;
            this.CarBrand.TabStop = false;
            this.CarBrand.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.CarBrand.TrailingIcon = null;
            this.CarBrand.UseSystemPasswordChar = false;
            // 
            // CarYear
            // 
            this.CarYear.AnimateReadOnly = false;
            this.CarYear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CarYear.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.CarYear.Depth = 0;
            this.CarYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.CarYear.HideSelection = true;
            this.CarYear.Hint = "Рік випуску машини";
            this.CarYear.LeadingIcon = null;
            this.CarYear.Location = new System.Drawing.Point(26, 107);
            this.CarYear.Margin = new System.Windows.Forms.Padding(4);
            this.CarYear.MaxLength = 32767;
            this.CarYear.MouseState = MaterialSkin.MouseState.OUT;
            this.CarYear.Name = "CarYear";
            this.CarYear.PasswordChar = '*';
            this.CarYear.PrefixSuffixText = null;
            this.CarYear.ReadOnly = false;
            this.CarYear.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CarYear.SelectedText = "";
            this.CarYear.SelectionLength = 0;
            this.CarYear.SelectionStart = 0;
            this.CarYear.ShortcutsEnabled = true;
            this.CarYear.Size = new System.Drawing.Size(259, 48);
            this.CarYear.TabIndex = 10;
            this.CarYear.TabStop = false;
            this.CarYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.CarYear.TrailingIcon = null;
            this.CarYear.UseSystemPasswordChar = false;
            // 
            // applicationsTabPage
            // 
            this.applicationsTabPage.ImageKey = "icons8-purchase-order-100.png";
            this.applicationsTabPage.Location = new System.Drawing.Point(4, 39);
            this.applicationsTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.applicationsTabPage.Name = "applicationsTabPage";
            this.applicationsTabPage.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.applicationsTabPage.Size = new System.Drawing.Size(967, 629);
            this.applicationsTabPage.TabIndex = 1;
            this.applicationsTabPage.Text = "Заявки";
            this.applicationsTabPage.UseVisualStyleBackColor = true;
            // 
            // usersTabPage
            // 
            this.usersTabPage.ImageKey = "icons8-people-100.png";
            this.usersTabPage.Location = new System.Drawing.Point(4, 39);
            this.usersTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.usersTabPage.Name = "usersTabPage";
            this.usersTabPage.Size = new System.Drawing.Size(967, 629);
            this.usersTabPage.TabIndex = 5;
            this.usersTabPage.Text = "Користувачі";
            this.usersTabPage.UseVisualStyleBackColor = true;
            // 
            // settingsTabPage
            // 
            this.settingsTabPage.ImageKey = "icons8-gear-100.png";
            this.settingsTabPage.Location = new System.Drawing.Point(4, 39);
            this.settingsTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.settingsTabPage.Name = "settingsTabPage";
            this.settingsTabPage.Size = new System.Drawing.Size(967, 629);
            this.settingsTabPage.TabIndex = 4;
            this.settingsTabPage.Text = "Налаштування";
            this.settingsTabPage.UseVisualStyleBackColor = true;
            // 
            // exitFromAdminTabPage
            // 
            this.exitFromAdminTabPage.ImageKey = "icons8-logout-100.png";
            this.exitFromAdminTabPage.Location = new System.Drawing.Point(4, 39);
            this.exitFromAdminTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.exitFromAdminTabPage.Name = "exitFromAdminTabPage";
            this.exitFromAdminTabPage.Size = new System.Drawing.Size(967, 629);
            this.exitFromAdminTabPage.TabIndex = 2;
            this.exitFromAdminTabPage.Text = "Вийти";
            this.exitFromAdminTabPage.UseVisualStyleBackColor = true;
            // 
            // imageListAdminForm
            // 
            this.imageListAdminForm.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListAdminForm.ImageStream")));
            this.imageListAdminForm.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListAdminForm.Images.SetKeyName(0, "icons8-logout-100.png");
            this.imageListAdminForm.Images.SetKeyName(1, "icons8-purchase-order-100.png");
            this.imageListAdminForm.Images.SetKeyName(2, "icons8-add-new-100.png");
            this.imageListAdminForm.Images.SetKeyName(3, "icons8-home-100.png");
            this.imageListAdminForm.Images.SetKeyName(4, "icons8-gear-100.png");
            this.imageListAdminForm.Images.SetKeyName(5, "icons8-people-100.png");
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(980, 738);
            this.Controls.Add(this.adminMenu);
            this.DrawerTabControl = this.adminMenu;
            this.Location = new System.Drawing.Point(15, 15);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AdminForm";
            this.Padding = new System.Windows.Forms.Padding(3, 64, 3, 2);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.AdminForm_Load);
            this.adminMenu.ResumeLayout(false);
            this.addCarTabPage.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        private MaterialSkin.Controls.MaterialMultiLineTextBox2 materialMultiLineTextBox21;

        private MaterialSkin.Controls.MaterialTextBox materialTextBox1;

        private MaterialSkin.Controls.MaterialMaskedTextBox register_phone_input;
        private MaterialSkin.Controls.MaterialButton register_btn;
        private MaterialSkin.Controls.MaterialTextBox2 PriceRent;
        private MaterialSkin.Controls.MaterialTextBox2 register_id_passport_input;
        private MaterialSkin.Controls.MaterialTextBox2 CarColor;
        private MaterialSkin.Controls.MaterialTextBox2 CarModel;
        private MaterialSkin.Controls.MaterialTextBox2 CarBrand;
        private MaterialSkin.Controls.MaterialTextBox2 CarYear;

        private System.Windows.Forms.TabPage usersTabPage;

        private System.Windows.Forms.TabPage settingsTabPage;

        private System.Windows.Forms.TabPage homeTabPage;

        private System.Windows.Forms.ImageList imageListAdminForm;

        private System.Windows.Forms.TabPage exitFromAdminTabPage;

        private MaterialSkin.Controls.MaterialTabControl adminMenu;
        private System.Windows.Forms.TabPage addCarTabPage;
        private System.Windows.Forms.TabPage applicationsTabPage;

        #endregion
    }
}