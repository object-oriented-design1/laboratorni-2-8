﻿namespace Laboratorni_2_8.Interfaces
{
    
    using System;
    
    interface IHuman
    {
        String Name { get; set; }
        String Surname { get; set; }
        String PhoneNumber { get; set; }
        String Email { get; set; }
        String IdPassport { get; set; }
        public String FullName { get; }
    }
}