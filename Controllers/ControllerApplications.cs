﻿namespace Laboratorni_2_8.Controllers
{
    
    using System.Collections.Generic;
    using System.Linq;
    using Interfaces;
    using Models;
    using Contexts;

    public class ControllerApplications : IDataBaseApplications
    {
     
        private LaboratoryDbContext _db = new LaboratoryDbContext();
        
        public void CreateNewApplication(ApplicationsModel applicationModel)
        {
            _db.ApplicationsDataBase.Add(applicationModel);
        }

        public ApplicationsModel SelectApplication(int idApplication)
        {
            return _db.ApplicationsDataBase.FirstOrDefault(app => app.Id == idApplication);
        }

        public List<ApplicationsModel> SelectApplications()
        {
            return _db.ApplicationsDataBase.ToList();
        }

        public void UpdateApplication(int idApplication, string status, string notes)
        {
            return;
        }

        public void UpdateApplication(int idApplication, string statusOrNotes)
        {
            return;
        }

        public UsersModel GetUserApplications(int idApplication)
        {
            return new UsersModel();
        }

        public CarsModel GetCarApplications(int idApplication)
        {
            return new CarsModel();
        }

        public ApplicationsModel GetApplicationsByStatus(string status)
        {
            return new ApplicationsModel();
        }
    }
}