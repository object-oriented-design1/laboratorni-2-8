﻿using System.Linq;
using Laboratorni_2_8.Classes;

namespace Laboratorni_2_8.Controllers
{
    using System;
    using Contexts;
    using Interfaces;
    using Models;
    using System.Collections.Generic;

    public class ControllerCars : IDataBaseCars
    {
        private static LaboratoryDbContext _db = new LaboratoryDbContext();

        public int Id { get; set; }
        public string Model { get; set; }
        public string Brand { get; set; }
        public string Year { get; set; }
        public string Color { get; set; }
        public string DescriptionCar { get; set; }
        public int Price { get; set; }
        public bool IsRent { get; set; }

        public void AddCar(IDataBaseCars car)
        {

            CarsModel newCarModel = new CarsModel();

            newCarModel.Model = car.Model;
            newCarModel.Brand = car.Brand;
            newCarModel.Year = car.Year;
            newCarModel.Color = car.Color;
            newCarModel.DescriptionCar = car.DescriptionCar;
            newCarModel.Price = car.Price;
            newCarModel.IsRent = false;
            
            _db.CarDataBase.Add(newCarModel);
            _db.SaveChanges();
        }

        public void DeleteCar(int idCar)
        {
            CarsModel carModel = _db.CarDataBase.FirstOrDefault(c => c.Id == idCar);
            if (carModel != null)
            {
                _db.CarDataBase.Remove(carModel);
                _db.SaveChanges();
            }
            else
            {
                throw new Exception("Такої машини не найдено");
            }
        }

        public void UpdateCar(int idCar, IDataBaseCars car)
        {
            CarsModel carDb = _db.CarDataBase.FirstOrDefault(c => c.Id == idCar);

            if (carDb == null) throw new Exception("Таку машину не було знайдено");
            
            carDb.Update(car);
            _db.SaveChanges();
        }

        public CarsModel SelectCar(int idCar)
        {
            return _db.CarDataBase.FirstOrDefault(c => c.Id == idCar);
        }

        public List<CarsModel> SelectCars()
        {
            return _db.CarDataBase.ToList();
        }
    }
}