﻿
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Laboratorni_2_8.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Laboratorni_2_8.Contexts.LaboratoryDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
    } 
}