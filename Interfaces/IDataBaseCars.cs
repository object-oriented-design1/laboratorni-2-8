﻿using Laboratorni_2_8.Models;

namespace Laboratorni_2_8.Interfaces
{
    
    using System.Collections.Generic;
    using System;
    using Classes;
    
    public interface IDataBaseCars
    {
        // Variables
        public int Id { get; set; }
        public String Model { get; set; }
        public String Brand { get; set; }
        public String Year { get; set; }
        public String Color { get; set; }
        public String DescriptionCar { get; set; }
        public int Price { get; set; }
        public bool IsRent { get; set; }
        
    }
}