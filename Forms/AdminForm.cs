﻿using System;
using Laboratorni_2_8.Classes;
using MaterialSkin;
using MaterialSkin.Controls;

namespace Laboratorni_2_8.Forms
{
    public partial class AdminForm : MaterialForm
    {
        private Administrator Administrator { get; set; }
        public AdminForm(Administrator admin)
        {
            InitializeComponent();
            
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.BlueGrey800,
                Primary.BlueGrey900,
                Primary.BlueGrey500,
                Accent.Blue700, 
                TextShade.WHITE
            );

            Administrator = admin;
        }

        private void ExitFromAdminForm(object sender, EventArgs e)
        {
            if (adminMenu.SelectedTab.Name == "exitFromUserTabPage")
            {
                new AuthForm().Show();
                Close();
            } 
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            Text = @"Вітаємо вас, " + Administrator.FullName;
        }
    }
}