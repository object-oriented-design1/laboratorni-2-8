﻿using Laboratorni_2_8.Interfaces;

namespace Laboratorni_2_8.Classes
{
    public class CarClass : IDataBaseCars
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public string Brand { get; set; }
        public string Year { get; set; }
        public string Color { get; set; }
        public string DescriptionCar { get; set; }
        public int Price { get; set; }
        public bool IsRent { get; set; }

        public CarClass(IDataBaseCars car)
        {
            Model = car.Model;
            Brand = car.Brand;
            Year = car.Year;
            Color = car.Color;
            DescriptionCar = car.DescriptionCar;
            Price = car.Price;
        }
    }
}