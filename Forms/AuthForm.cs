﻿using System;
using System.Windows.Forms;
using Laboratorni_2_8.Classes;
using Laboratorni_2_8.Controllers;
using Laboratorni_2_8.Models;
using MaterialSkin;
using MaterialSkin.Controls;

namespace Laboratorni_2_8.Forms
{
    public partial class AuthForm : MaterialForm
    {
        private readonly ControllerUsers _controllerUsers = new ControllerUsers();
        public AuthForm()
        {
            InitializeComponent();
            
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.BlueGrey800,
                Primary.BlueGrey900,
                Primary.BlueGrey500,
                Accent.Blue700, 
                TextShade.WHITE
                );
        }

        private void login_btn_Click(object sender, EventArgs e)
        {
            String login = login_input.Text, password = password_input.Text;
            
            if (_controllerUsers.UserExists(login, password))
            {
                UsersModel userModelDb = _controllerUsers.SelectUser(login, password);
                Client user = new Client(userModelDb);
                user.ShowInfo();

                if (!user.IsAdmin)
                {
                    UserForm userForm = new UserForm(user);
                    this.Hide();
                    userForm.ShowDialog();
                }
                else
                {
                    AdminForm adminForm = new AdminForm(new Administrator(userModelDb));
                    this.Hide();
                    adminForm.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show(@"Невірний логін або пароль", 
                    @"Помилка авторизації", 
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private void register_btn_Click(object sender, EventArgs e)
        {
            String 
                login = register_login_input.Text,
                password = register_password_input.Text,
                name = register_first_name_input.Text,
                surname = register_last_name_input.Text,
                email = register_email_input.Text,
                phone = register_phone_input.Text,
                idPassport = register_id_passport_input.Text;

            if (login.Length != 0 && 
                password.Length != 0 && 
                name.Length != 0 && 
                surname.Length != 0 &&
                email.Length != 0 && 
                idPassport.Length != 0 && 
                phone.Length != 15
               )
                MessageBox.Show(
                    @"Введіть коректно дані", 
                    @"Авторизація",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

            if (!_controllerUsers.LoginUserExists(login))
            {
                UsersModel userModel = _controllerUsers.AddUser(login, password, name, surname, email, phone, idPassport);
                Client client = new Client(userModel);
                client.ShowInfo();

                MessageBox.Show(
                    @"Ви успішно зареєструвалися." +
                    @"Верніться в вікно логіну щоб продовжити роботу.", 
                    @"Авторизація",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(
                    @"Користувач з таким логіном вже існує", 
                    @"Авторизація",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}