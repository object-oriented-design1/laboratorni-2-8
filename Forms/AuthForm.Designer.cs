﻿namespace Laboratorni_2_8.Forms
{
    partial class AuthForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthForm));
            this.materialTabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            this.login_tabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.login_btn = new MaterialSkin.Controls.MaterialButton();
            this.password_input = new MaterialSkin.Controls.MaterialTextBox2();
            this.login_input = new MaterialSkin.Controls.MaterialTextBox2();
            this.register_tabPage = new System.Windows.Forms.TabPage();
            this.register_phone_input = new MaterialSkin.Controls.MaterialMaskedTextBox();
            this.register_btn = new MaterialSkin.Controls.MaterialButton();
            this.register_email_input = new MaterialSkin.Controls.MaterialTextBox2();
            this.register_id_passport_input = new MaterialSkin.Controls.MaterialTextBox2();
            this.register_last_name_input = new MaterialSkin.Controls.MaterialTextBox2();
            this.register_login_input = new MaterialSkin.Controls.MaterialTextBox2();
            this.register_first_name_input = new MaterialSkin.Controls.MaterialTextBox2();
            this.register_password_input = new MaterialSkin.Controls.MaterialTextBox2();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.materialTabControl1.SuspendLayout();
            this.login_tabPage.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.register_tabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // materialTabControl1
            // 
            this.materialTabControl1.Controls.Add(this.login_tabPage);
            this.materialTabControl1.Controls.Add(this.register_tabPage);
            this.materialTabControl1.Depth = 0;
            this.materialTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialTabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.materialTabControl1.ImageList = this.imageList1;
            this.materialTabControl1.Location = new System.Drawing.Point(0, 64);
            this.materialTabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl1.Multiline = true;
            this.materialTabControl1.Name = "materialTabControl1";
            this.materialTabControl1.SelectedIndex = 0;
            this.materialTabControl1.Size = new System.Drawing.Size(438, 321);
            this.materialTabControl1.TabIndex = 0;
            // 
            // login_tabPage
            // 
            this.login_tabPage.Controls.Add(this.tableLayoutPanel1);
            this.login_tabPage.ImageKey = "icons8-enter-100.png";
            this.login_tabPage.Location = new System.Drawing.Point(4, 39);
            this.login_tabPage.Name = "login_tabPage";
            this.login_tabPage.Padding = new System.Windows.Forms.Padding(3);
            this.login_tabPage.Size = new System.Drawing.Size(430, 278);
            this.login_tabPage.TabIndex = 0;
            this.login_tabPage.Text = "Логін";
            this.login_tabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.login_btn, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.password_input, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.login_input, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(60);
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(424, 272);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // login_btn
            // 
            this.login_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.login_btn.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.login_btn.Depth = 0;
            this.login_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.login_btn.HighEmphasis = true;
            this.login_btn.Icon = null;
            this.login_btn.Location = new System.Drawing.Point(64, 174);
            this.login_btn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.login_btn.MouseState = MaterialSkin.MouseState.HOVER;
            this.login_btn.Name = "login_btn";
            this.login_btn.NoAccentTextColor = System.Drawing.Color.Empty;
            this.login_btn.Size = new System.Drawing.Size(296, 36);
            this.login_btn.TabIndex = 3;
            this.login_btn.Text = "Увійти";
            this.login_btn.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.login_btn.UseAccentColor = false;
            this.login_btn.UseVisualStyleBackColor = true;
            this.login_btn.Click += new System.EventHandler(this.login_btn_Click);
            // 
            // password_input
            // 
            this.password_input.AnimateReadOnly = false;
            this.password_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.password_input.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.password_input.Depth = 0;
            this.password_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.password_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.password_input.HideSelection = true;
            this.password_input.Hint = "Пароль";
            this.password_input.LeadingIcon = null;
            this.password_input.Location = new System.Drawing.Point(63, 117);
            this.password_input.MaxLength = 32767;
            this.password_input.MouseState = MaterialSkin.MouseState.OUT;
            this.password_input.Name = "password_input";
            this.password_input.PasswordChar = '*';
            this.password_input.PrefixSuffixText = null;
            this.password_input.ReadOnly = false;
            this.password_input.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.password_input.SelectedText = "";
            this.password_input.SelectionLength = 0;
            this.password_input.SelectionStart = 0;
            this.password_input.ShortcutsEnabled = true;
            this.password_input.Size = new System.Drawing.Size(298, 48);
            this.password_input.TabIndex = 2;
            this.password_input.TabStop = false;
            this.password_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.password_input.TrailingIcon = null;
            this.password_input.UseSystemPasswordChar = false;
            // 
            // login_input
            // 
            this.login_input.AnimateReadOnly = false;
            this.login_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.login_input.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.login_input.Depth = 0;
            this.login_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.login_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.login_input.HideSelection = true;
            this.login_input.Hint = "Логін";
            this.login_input.LeadingIcon = null;
            this.login_input.Location = new System.Drawing.Point(63, 63);
            this.login_input.MaxLength = 32767;
            this.login_input.MouseState = MaterialSkin.MouseState.OUT;
            this.login_input.Name = "login_input";
            this.login_input.PasswordChar = '\0';
            this.login_input.PrefixSuffixText = null;
            this.login_input.ReadOnly = false;
            this.login_input.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.login_input.SelectedText = "";
            this.login_input.SelectionLength = 0;
            this.login_input.SelectionStart = 0;
            this.login_input.ShortcutsEnabled = true;
            this.login_input.Size = new System.Drawing.Size(298, 48);
            this.login_input.TabIndex = 1;
            this.login_input.TabStop = false;
            this.login_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.login_input.TrailingIcon = null;
            this.login_input.UseSystemPasswordChar = false;
            // 
            // register_tabPage
            // 
            this.register_tabPage.Controls.Add(this.register_phone_input);
            this.register_tabPage.Controls.Add(this.register_btn);
            this.register_tabPage.Controls.Add(this.register_email_input);
            this.register_tabPage.Controls.Add(this.register_id_passport_input);
            this.register_tabPage.Controls.Add(this.register_last_name_input);
            this.register_tabPage.Controls.Add(this.register_login_input);
            this.register_tabPage.Controls.Add(this.register_first_name_input);
            this.register_tabPage.Controls.Add(this.register_password_input);
            this.register_tabPage.ImageKey = "icons8-register-100.png";
            this.register_tabPage.Location = new System.Drawing.Point(4, 39);
            this.register_tabPage.Name = "register_tabPage";
            this.register_tabPage.Padding = new System.Windows.Forms.Padding(3);
            this.register_tabPage.Size = new System.Drawing.Size(430, 278);
            this.register_tabPage.TabIndex = 1;
            this.register_tabPage.Text = "Реєстрація";
            this.register_tabPage.UseVisualStyleBackColor = true;
            // 
            // register_phone_input
            // 
            this.register_phone_input.AllowPromptAsInput = true;
            this.register_phone_input.AnimateReadOnly = true;
            this.register_phone_input.AsciiOnly = false;
            this.register_phone_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.register_phone_input.BeepOnError = false;
            this.register_phone_input.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.IncludeLiterals;
            this.register_phone_input.Depth = 0;
            this.register_phone_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.register_phone_input.HidePromptOnLeave = false;
            this.register_phone_input.HideSelection = true;
            this.register_phone_input.Hint = "Номер телефону";
            this.register_phone_input.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Default;
            this.register_phone_input.LeadingIcon = null;
            this.register_phone_input.Location = new System.Drawing.Point(6, 211);
            this.register_phone_input.Mask = "(000)-000-00-00";
            this.register_phone_input.MaxLength = 32767;
            this.register_phone_input.MouseState = MaterialSkin.MouseState.OUT;
            this.register_phone_input.Name = "register_phone_input";
            this.register_phone_input.PasswordChar = '\0';
            this.register_phone_input.PrefixSuffix = MaterialSkin.Controls.MaterialMaskedTextBox.PrefixSuffixTypes.Prefix;
            this.register_phone_input.PrefixSuffixText = "+380";
            this.register_phone_input.PromptChar = '_';
            this.register_phone_input.ReadOnly = false;
            this.register_phone_input.RejectInputOnFirstFailure = false;
            this.register_phone_input.ResetOnPrompt = true;
            this.register_phone_input.ResetOnSpace = true;
            this.register_phone_input.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.register_phone_input.SelectedText = "";
            this.register_phone_input.SelectionLength = 0;
            this.register_phone_input.SelectionStart = 0;
            this.register_phone_input.ShortcutsEnabled = true;
            this.register_phone_input.Size = new System.Drawing.Size(194, 48);
            this.register_phone_input.SkipLiterals = true;
            this.register_phone_input.TabIndex = 7;
            this.register_phone_input.TabStop = false;
            this.register_phone_input.Text = "(   )-   -  -";
            this.register_phone_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.register_phone_input.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludeLiterals;
            this.register_phone_input.TrailingIcon = null;
            this.register_phone_input.UseSystemPasswordChar = false;
            this.register_phone_input.ValidatingType = null;
            // 
            // register_btn
            // 
            this.register_btn.AutoSize = false;
            this.register_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.register_btn.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Dense;
            this.register_btn.Depth = 0;
            this.register_btn.HighEmphasis = true;
            this.register_btn.Icon = null;
            this.register_btn.Location = new System.Drawing.Point(228, 211);
            this.register_btn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.register_btn.MouseState = MaterialSkin.MouseState.HOVER;
            this.register_btn.Name = "register_btn";
            this.register_btn.NoAccentTextColor = System.Drawing.Color.Empty;
            this.register_btn.Size = new System.Drawing.Size(195, 48);
            this.register_btn.TabIndex = 8;
            this.register_btn.Text = "Реєстрація";
            this.register_btn.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.register_btn.UseAccentColor = false;
            this.register_btn.UseVisualStyleBackColor = true;
            this.register_btn.Click += new System.EventHandler(this.register_btn_Click);
            // 
            // register_email_input
            // 
            this.register_email_input.AnimateReadOnly = false;
            this.register_email_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.register_email_input.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.register_email_input.Depth = 0;
            this.register_email_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.register_email_input.HideSelection = true;
            this.register_email_input.Hint = "Email";
            this.register_email_input.LeadingIcon = null;
            this.register_email_input.Location = new System.Drawing.Point(6, 143);
            this.register_email_input.MaxLength = 32767;
            this.register_email_input.MouseState = MaterialSkin.MouseState.OUT;
            this.register_email_input.Name = "register_email_input";
            this.register_email_input.PasswordChar = '\0';
            this.register_email_input.PrefixSuffixText = null;
            this.register_email_input.ReadOnly = false;
            this.register_email_input.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.register_email_input.SelectedText = "";
            this.register_email_input.SelectionLength = 0;
            this.register_email_input.SelectionStart = 0;
            this.register_email_input.ShortcutsEnabled = true;
            this.register_email_input.Size = new System.Drawing.Size(194, 48);
            this.register_email_input.TabIndex = 5;
            this.register_email_input.TabStop = false;
            this.register_email_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.register_email_input.TrailingIcon = null;
            this.register_email_input.UseSystemPasswordChar = false;
            // 
            // register_id_passport_input
            // 
            this.register_id_passport_input.AnimateReadOnly = false;
            this.register_id_passport_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.register_id_passport_input.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.register_id_passport_input.Depth = 0;
            this.register_id_passport_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.register_id_passport_input.HideSelection = true;
            this.register_id_passport_input.Hint = "ID Паспорта";
            this.register_id_passport_input.LeadingIcon = null;
            this.register_id_passport_input.Location = new System.Drawing.Point(228, 143);
            this.register_id_passport_input.MaxLength = 32767;
            this.register_id_passport_input.MouseState = MaterialSkin.MouseState.OUT;
            this.register_id_passport_input.Name = "register_id_passport_input";
            this.register_id_passport_input.PasswordChar = '\0';
            this.register_id_passport_input.PrefixSuffixText = null;
            this.register_id_passport_input.ReadOnly = false;
            this.register_id_passport_input.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.register_id_passport_input.SelectedText = "";
            this.register_id_passport_input.SelectionLength = 0;
            this.register_id_passport_input.SelectionStart = 0;
            this.register_id_passport_input.ShortcutsEnabled = true;
            this.register_id_passport_input.Size = new System.Drawing.Size(196, 48);
            this.register_id_passport_input.TabIndex = 6;
            this.register_id_passport_input.TabStop = false;
            this.register_id_passport_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.register_id_passport_input.TrailingIcon = null;
            this.register_id_passport_input.UseSystemPasswordChar = false;
            // 
            // register_last_name_input
            // 
            this.register_last_name_input.AnimateReadOnly = false;
            this.register_last_name_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.register_last_name_input.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.register_last_name_input.Depth = 0;
            this.register_last_name_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.register_last_name_input.HideSelection = true;
            this.register_last_name_input.Hint = "Прізвище";
            this.register_last_name_input.LeadingIcon = null;
            this.register_last_name_input.Location = new System.Drawing.Point(228, 78);
            this.register_last_name_input.MaxLength = 32767;
            this.register_last_name_input.MouseState = MaterialSkin.MouseState.OUT;
            this.register_last_name_input.Name = "register_last_name_input";
            this.register_last_name_input.PasswordChar = '\0';
            this.register_last_name_input.PrefixSuffixText = null;
            this.register_last_name_input.ReadOnly = false;
            this.register_last_name_input.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.register_last_name_input.SelectedText = "";
            this.register_last_name_input.SelectionLength = 0;
            this.register_last_name_input.SelectionStart = 0;
            this.register_last_name_input.ShortcutsEnabled = true;
            this.register_last_name_input.Size = new System.Drawing.Size(196, 48);
            this.register_last_name_input.TabIndex = 4;
            this.register_last_name_input.TabStop = false;
            this.register_last_name_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.register_last_name_input.TrailingIcon = null;
            this.register_last_name_input.UseSystemPasswordChar = false;
            // 
            // register_login_input
            // 
            this.register_login_input.AnimateReadOnly = false;
            this.register_login_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.register_login_input.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.register_login_input.Depth = 0;
            this.register_login_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.register_login_input.HideSelection = true;
            this.register_login_input.Hint = "Логін";
            this.register_login_input.LeadingIcon = null;
            this.register_login_input.Location = new System.Drawing.Point(6, 15);
            this.register_login_input.MaxLength = 32767;
            this.register_login_input.MouseState = MaterialSkin.MouseState.OUT;
            this.register_login_input.Name = "register_login_input";
            this.register_login_input.PasswordChar = '\0';
            this.register_login_input.PrefixSuffixText = null;
            this.register_login_input.ReadOnly = false;
            this.register_login_input.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.register_login_input.SelectedText = "";
            this.register_login_input.SelectionLength = 0;
            this.register_login_input.SelectionStart = 0;
            this.register_login_input.ShortcutsEnabled = true;
            this.register_login_input.Size = new System.Drawing.Size(194, 48);
            this.register_login_input.TabIndex = 1;
            this.register_login_input.TabStop = false;
            this.register_login_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.register_login_input.TrailingIcon = null;
            this.register_login_input.UseSystemPasswordChar = false;
            // 
            // register_first_name_input
            // 
            this.register_first_name_input.AnimateReadOnly = false;
            this.register_first_name_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.register_first_name_input.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.register_first_name_input.Depth = 0;
            this.register_first_name_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.register_first_name_input.HideSelection = true;
            this.register_first_name_input.Hint = "Ім\'я";
            this.register_first_name_input.LeadingIcon = null;
            this.register_first_name_input.Location = new System.Drawing.Point(228, 15);
            this.register_first_name_input.MaxLength = 32767;
            this.register_first_name_input.MouseState = MaterialSkin.MouseState.OUT;
            this.register_first_name_input.Name = "register_first_name_input";
            this.register_first_name_input.PasswordChar = '\0';
            this.register_first_name_input.PrefixSuffixText = null;
            this.register_first_name_input.ReadOnly = false;
            this.register_first_name_input.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.register_first_name_input.SelectedText = "";
            this.register_first_name_input.SelectionLength = 0;
            this.register_first_name_input.SelectionStart = 0;
            this.register_first_name_input.ShortcutsEnabled = true;
            this.register_first_name_input.Size = new System.Drawing.Size(196, 48);
            this.register_first_name_input.TabIndex = 3;
            this.register_first_name_input.TabStop = false;
            this.register_first_name_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.register_first_name_input.TrailingIcon = null;
            this.register_first_name_input.UseSystemPasswordChar = false;
            // 
            // register_password_input
            // 
            this.register_password_input.AnimateReadOnly = false;
            this.register_password_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.register_password_input.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.register_password_input.Depth = 0;
            this.register_password_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.register_password_input.HideSelection = true;
            this.register_password_input.Hint = "Пароль";
            this.register_password_input.LeadingIcon = null;
            this.register_password_input.Location = new System.Drawing.Point(6, 78);
            this.register_password_input.MaxLength = 32767;
            this.register_password_input.MouseState = MaterialSkin.MouseState.OUT;
            this.register_password_input.Name = "register_password_input";
            this.register_password_input.PasswordChar = '*';
            this.register_password_input.PrefixSuffixText = null;
            this.register_password_input.ReadOnly = false;
            this.register_password_input.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.register_password_input.SelectedText = "";
            this.register_password_input.SelectionLength = 0;
            this.register_password_input.SelectionStart = 0;
            this.register_password_input.ShortcutsEnabled = true;
            this.register_password_input.Size = new System.Drawing.Size(194, 48);
            this.register_password_input.TabIndex = 2;
            this.register_password_input.TabStop = false;
            this.register_password_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.register_password_input.TrailingIcon = null;
            this.register_password_input.UseSystemPasswordChar = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "icons8-enter-100.png");
            this.imageList1.Images.SetKeyName(1, "icons8-register-100.png");
            // 
            // AuthForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 388);
            this.Controls.Add(this.materialTabControl1);
            this.DrawerTabControl = this.materialTabControl1;
            this.Name = "AuthForm";
            this.Padding = new System.Windows.Forms.Padding(0, 64, 3, 3);
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авторизація";
            this.materialTabControl1.ResumeLayout(false);
            this.login_tabPage.ResumeLayout(false);
            this.login_tabPage.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.register_tabPage.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        private MaterialSkin.Controls.MaterialTextBox2 register_email_input;
        private MaterialSkin.Controls.MaterialButton register_btn;
        private MaterialSkin.Controls.MaterialMaskedTextBox register_phone_input;

        private MaterialSkin.Controls.MaterialTextBox2 register_id_passport_input;
        private MaterialSkin.Controls.MaterialTextBox2 register_last_name_input;

        private MaterialSkin.Controls.MaterialTextBox2 register_password_input;

        private MaterialSkin.Controls.MaterialTextBox2 register_first_name_input;
        private MaterialSkin.Controls.MaterialTextBox2 register_login_input;

        private MaterialSkin.Controls.MaterialButton login_btn;

        private MaterialSkin.Controls.MaterialTextBox2 login_input;
        private MaterialSkin.Controls.MaterialTextBox2 password_input;

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

        private System.Windows.Forms.ImageList imageList1;

        private MaterialSkin.Controls.MaterialTabControl materialTabControl1;
        private System.Windows.Forms.TabPage login_tabPage;
        private System.Windows.Forms.TabPage register_tabPage;

        #endregion
    }
}