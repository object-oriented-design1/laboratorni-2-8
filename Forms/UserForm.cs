﻿using System;
using Laboratorni_2_8.Classes;
using MaterialSkin;
using MaterialSkin.Controls;

namespace Laboratorni_2_8.Forms
{
    public partial class UserForm : MaterialForm
    {
        private Client Client { get; set; }
        public UserForm(Client client)
        {
            InitializeComponent();
            
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.BlueGrey800,
                Primary.BlueGrey900,
                Primary.BlueGrey500,
                Accent.Blue700, 
                TextShade.WHITE
            );

            Client = client;
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            Text = @"Вітаємо вас, " + Client.FullName;
        }

        private void ExitFromUserForm(object sender, EventArgs e)
        {
            if (userMenu.SelectedTab.Name == "exitFromAdminTabPage") 
            {
                new AuthForm().Show();
                Close();
            }
        }
    }
}